### Matthew Eichler, Aventine Solutions
## Curriculum Vitae and Project Details Documents

In various formats

* DOC - Microsoft Office 97 - 2003
* DOCX - Microsoft Office 2007 - 365
* ODT - Libre Office Open Document Text
* PDF - Adobe Portable Document Format
